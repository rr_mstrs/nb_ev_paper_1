#!/bin/bash

# # Bail on script errors
# set -e

###################################################################################################
# To manage which parts of the workflow to run
###################################################################################################


WEEKS_2019=(1-APR-2019 8-APR-2019 15-APR-2019 22-APR-2019 29-APR-2019 6-MAY-2019 13-MAY-2019 20-MAY-2019 27-MAY-2019 3-JUN-2019 10-JUN-2019 17-JUN-2019 24-JUN-2019 1-JUL-2019 8-JUL-2019 15-JUL-2019 22-JUL-2019 29-JUL-2019 5-AUG-2019 12-AUG-2019 19-AUG-2019 26-AUG-2019 29-AUG-2019 9-SEP-2019 16-SEP-2019 23-SEP-2019 30-SEP-2019 7-OCT-2019 14-OCT-2019 21-OCT-2019 28-OCT-2019 4-NOV-2019 11-NOV-2019 18-NOV-2019 25-NOV-2019 2-DEC-2019 9-DEC-2019 16-DEC-2019 23-DEC-2019)
SEASONS_2019=(winter spring summer fall)
MONTHS_2019=(4 5 6 7 8 9 10 11 12)

WEEKS_2020=(30-DEC-2019 6-JAN-2020 13-JAN-2020 20-JAN-2020 27-JAN-2020 3-FEB-2020 10-FEB-2020 17-FEB-2020 24-FEB-2020 2-MAR-2020 9-MAR-2020 16-MAR-2020 23-MAR-2020 30-MAR-2020 6-APR-2020 13-APR-2020 20-APR-2020 27-APR-2020)
SEASONS_2020=(winter spring)
MONTHS_2020=(1 2 3 4)

TRACK_EXPERIMENTS=True
REDIR_OUT=True


DO_ONE_WAY_HASH=true


WEEKLY_DO_CREATE_BACH_RANGES=true
MONTHLY_DO_CREATE_BACH_RANGES=true
SEASONAL_DO_CREATE_BACH_RANGES=true


###################################################################################################
# Workflow function definitions (SAMPLE)
###################################################################################################


one_way_hash() {
    if [ "${DO_ONE_WAY_HASH}" = true ]; then
        echo "Running one_way_hash"
        echo "..."
        python ./pre_processing/one_way_hash.py \
        --master local[19] \
        --driver_memory 2g \
        --executor_memory 18g \
        --track_experiment $TRACK_EXPERIMENTS  \
        --redir_out $REDIR_OUT \
        --experiment one_way_hash \
        --i_recharge_rpt "${HOME}/data/nb_power/raw/*/*/recharge.csv" \
        --i_transact_rpt "${HOME}/data/nb_power/raw/*/*/transaction.csv" \
        --i_util_rpt "${HOME}/data/nb_power/raw/*/*/utilization.csv" \
        --o_recharge_rpt "${HOME}"/data/nb_power/work/hashed_recharge_report.parquet \
        --o_transact_rpt "${HOME}"/data/nb_power/work/hashed_transactions_report.parquet \
        --o_util_rpt "${HOME}"/data/nb_power/work/hashed_utilization_report.parquet
        echo "..."
    fi
}

create_batch_ranges() {
    echo "Running create_batch_ranges on recharge report files"
    echo Year : "${YEAR}", Season : "${SEASON}", Month : "${MONTH}", Week : "${WEEK}"
    echo "..."
    python ./pre_processing/create_batch_ranges.py \
    --master local[19] \
    --driver_memory 2g \
    --executor_memory 18g \
    --track_experiment $TRACK_EXPERIMENTS  \
    --redir_out $REDIR_OUT \
    --experiment create_batch_ranges \
    --i_input "${HOME}"/data/nb_power/work/feat_eng_rech_report.parquet \
    --o_output_dir "${HOME}"/data/nb_power/work/ \
    --i_year $YEAR \
    --i_season $SEASON \
    --i_month $MONTH \
    --i_week_starting $WEEK
    echo "..."
}





###################################################################################################
# Workflow Execution 
###################################################################################################

one_way_hash




run_weeky_batch_ranges(){
    if [ "${WEEKLY_DO_CREATE_BACH_RANGES}" = true ]; then
        echo "Running weekly batch range creation"
        YEAR=2019
        SEASON=na
        MONTH=na
        for WEEK in "${WEEKS_2019[@]}"
        do
            create_batch_ranges
        done

        YEAR=2020
        SEASON=na
        MONTH=na
        for WEEK in "${WEEKS_2020[@]}"
        do
            create_batch_ranges
        done
    fi
}
run_weeky_batch_ranges

run_monthly_batch_ranges(){
    if [ "${MONTHLY_DO_CREATE_BACH_RANGES}" = true ]; then
        echo "Running monthly batch range creation"
        YEAR=2019
        SEASON=na
        WEEK=na
        for MONTH in "${MONTHS_2019[@]}"
        do
            create_batch_ranges
        done

        YEAR=2020
        SEASON=na
        WEEK=na
        for MONTH in "${MONTHS_2020[@]}"
        do
            create_batch_ranges
        done
    fi
}
run_monthly_batch_ranges

run_seasonal_batch_ranges(){
    if [ "${SEASONAL_DO_CREATE_BACH_RANGES}" = true ]; then
        echo "Running seasonal batch range creation"
        YEAR=2019
        MONTH=na
        WEEK=na
        for SEASON in "${SEASONS_2019[@]}"
        do
            create_batch_ranges
        done

        YEAR=2020
        MONTH=na
        WEEK=na
        for SEASON in "${SEASONS_2020[@]}"
        do
            create_batch_ranges
        done
    fi
}
run_seasonal_batch_ranges
