# README #

### What is this repository for? ###
 
* Sample workflow execution code. 
* Workflow elements were executed in sequence using shell commands that called parameterized Python scripts. 
* `sample_workflow_elements.sh` provides an example of executing workflow steps  1  and  5.


### Who do I talk to? ###

* rene.richard@unb.ca